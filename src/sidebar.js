var qlconfig_sidebar =
[
    {
        text: "Setup",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: "Master",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            } 
        ]
    },
    {
        text: "Purchasing",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: "Master",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: "Transaction",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]
    }, {
        text: "Inventory",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: "Master",
                icon: "cil-star",
                expanded: false,
                children: []
            }, {
                text: "Transaction",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]

    },{
        text: "Production",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: "Master",
                icon: "cil-star",
                expanded: false,
                children: []
            }, {
                text: "Transaction",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]

    },
    {
        text: "Marketing",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: "Master",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: "Transaction",
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]
    },
    {
        text: "Accounting",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: 'Master',
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Transaction',
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]
    },
    {
        text: "Fixed Asset",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
                text: 'Master',
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Transaction',
                icon: "cil-star",
                expanded: false,
                children: []
            },
            {
                text: 'Report',
                icon: "cil-star",
                expanded: false,
                children: []
            }
        ]
    },
    {
        text: "Templates",
        icon: "cil-bookmark",
        expanded: false,
        children: [{
            text: "Transaction",
            icon: "cil-star",
            expanded: false,
            children: []
        }]
    }
];

export default qlconfig_sidebar;