const rawData = {
    "json_version": 2,
    "process": "Register",
    "forms": [
        {
            "name": "register",
            "type": "Header",
            "data": [
                {
                    "id": "name",
                    "label-name-type": "Name*-*name*-*text",
                    "ch-req-ro-dis-hd": "true*-*true*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "email",
                    "label-name-type": "Email*-*email*-*text",
                    "ch-req-ro-dis-hd": "true*-*true*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "password",
                    "label-name-type": "Password*-*password*-*text",
                    "ch-req-ro-dis-hd": "true*-*true*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "confirmation_password",
                    "label-name-type": "Confirmation Password*-*confirmation_password*-*text",
                    "ch-req-ro-dis-hd": "true*-*true*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                }
            ],
            "noform": false,
            "popup": false
        }
    ]
}
export default rawData