import Vue from 'vue'
import Vuex from 'vuex'
import qlconfig_sidebar from "./sidebar";
Vue.use(Vuex)

const state = {
  sidebarShow: 'responsive',
  sidebarMinimize: false,
  asideShow: false,
  darkMode: true,
  globalModal:false,
  sidebar:qlconfig_sidebar,
  responsibility:{},
  apiHeader:{},
  user:{}, //user
  routesAllowed:[], //roles
  routes:[{
    path: "dashboard",
    name: "Dashboard",
    component: () => import("@/views/Dashboard.vue"),
  },{
      path: "notifications",
      name: "Notifications",
      component: () => import("@/views/Notifications.vue"),
  }]
}

const mutations = {
  toggleSidebarDesktop (state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  toggleSidebarMobile (state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set (state, [variable, value]) {
    state[variable] = value
  },
  toggle (state, variable) {
    state[variable] = !state[variable]
  },
  toggleModal ( state ) {
    state.globalModal = !state.globalModal;
  },
  addRoute(state,dataArray){
    dataArray.forEach(data=>{
      state.routes.push(data);
    })
  },
}

const actions={  
  registerMenuExecution({commit,state}){
      let menuArray = state.routes;
      let menuSidebar={};
      let general = {
          seq:-99999,
          text: "General",
          iconCls: "x-fa fa-bookmark",
          xtype: "set_mas_general",
          expanded: false,
          children: []
      };
      let generalChildren = [];
      menuArray.forEach(function(dt){
          if(["notifications","dashboard"].includes( (dt.name).toLowerCase()) || dt.path.includes(":id")){return}
          if(dt.childmodul!==undefined && (dt.childmodul).toLowerCase()=='general'){
              generalChildren.push(dt);
          }else{
              if(menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase()]===undefined){
                  menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase()]=[];
              }
              menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase() ].push(dt);
          }
      });
      if(generalChildren.length>0){
          generalChildren = generalChildren.sort( function(a,b){return a.seq-b.seq } );
          general.children=generalChildren;
          menuSidebar['setup-master'].push(general);
      }
      for(let label in menuSidebar){
          menuSidebar[label].sort( function(a,b){return a.seq-b.seq } );
          let modul = label.split('-')[0];
          let submodul = label.split('-')[1];
          let modulIndex = qlconfig_sidebar.findIndex(function(dt){
              return (dt.text).toLowerCase() == modul;
          });
          let subModulIndex = (qlconfig_sidebar[modulIndex].children).findIndex(function(dt){
              return (dt.text).toLowerCase() == submodul;
          });
          qlconfig_sidebar[modulIndex].children[subModulIndex].children = menuSidebar[label];
      }
  }
}
export default new Vuex.Store({
  state,
  mutations,
  actions
})