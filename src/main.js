import 'core-js/stable'
import Vue from 'vue'
// import CoreuiVuePro from '@coreui/vue-pro'
import CoreuiVuePro from '../node_modules/@coreui/vue-pro/src/index.js'
import App from './App'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import router from './router/index'
import { freeSet as icons } from '@coreui/icons'
import store from './store'
import i18n from './i18n.js'

Vue.use(CoreuiVuePro)
Vue.prototype.$log = console.log.bind(console)

import mixin from  './mixin.js'
import mixin1 from  './localmixins/mixin1'
import mixin2 from  './localmixins/mixin2'
import mixin3 from  './localmixins/mixin3'
import mixin4 from  './localmixins/mixin4'
import mixin5 from  './localmixins/mixin5'
import mixin6 from  './localmixins/mixin6'
import mixin7 from  './localmixins/mixin7'
import mixin8 from  './localmixins/mixin8'
import Numberfield from '@/elements/Numberfield'
import Numberfield2 from '@/elements/Numberfield2'
import Uploadfield from '@/elements/Uploadfield'
import Popupfield from '@/elements/Popupfield'
import Selectfield from '@/elements/Selectfield'
import Timefield from '@/elements/Timefield'
import Ratingfield from '@/elements/Ratingfield'
import Tagfield from '@/elements/Tagfield'
import Spinnerfield from '@/elements/Spinnerfield'
import QDataTable from '@/elements/QDataTable'
import Datatable from '@/elements/Datatable'
import Datefield from '@/elements/Datefield'
import Textfield from '@/elements/Textfield'
import Radiofield from '@/elements/Radiofield'
import Checkboxfield from '@/elements/Checkboxfield'
import Passwordfield from '@/elements/Passwordfield'
import Textnumberfield from '@/elements/Textnumberfield'
import Domainfield from '@/elements/Domainfield'
import Textareafield from '@/elements/Textareafield'
import Emailfield from '@/elements/Emailfield'
import Space from '@/elements/Space'
import Form from '@/elements/Form'
import Detail from '@/elements/Detail'
import DatatableMulti from '@/elements/DatatableMulti'
import Preview from '@/elements/Preview'
import VModal from 'vue-js-modal'
import browserDetect from "vue-browser-detect-plugin";
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

router.afterEach((to,from)=>{
  let a = Array.from(document.getElementsByClassName('c-sidebar-nav-link'));
  for(let i in a){
    a[i].classList.remove("router-link-exact-active"); 
    a[i].classList.remove("c-active");      
  }
  if( (to.path).split("/").length>3){
    for(let i in a){
      if ( ('#'+to.path).includes(a[i].getAttribute('href')) ){
        setTimeout(function(){
          a[i].classList.add("router-link-exact-active"); 
          a[i].classList.add("c-active");
        },1000)
        break;
      }
    }
  }
})
Vue.use(Loading,{
  // loader:'dots',
  // backgroundColor: '#28292e',
});
import VueAlertify from 'vue-alertify';
import {
  ValidationObserver,
  ValidationProvider,
  extend,
  localize
} from "vee-validate";
import en from "vee-validate/dist/locale/en.json";
import * as rules from "vee-validate/dist/rules";
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
Vue.use(VModal)
Vue.use(VueAlertify);
Vue.use(BootstrapVue)
Vue.use(browserDetect);

Vue.component('v-select', vSelect)
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);

Vue.component('Space',Space);
Vue.component('Emailfield',Emailfield);
Vue.component('Textareafield',Textareafield);
Vue.component('Domainfield',Domainfield);
Vue.component('Textnumberfield',Textnumberfield);
Vue.component('Passwordfield',Passwordfield);
Vue.component('Checkboxfield',Checkboxfield);
Vue.component('Radiofield',Radiofield);
Vue.component('Textfield',Textfield);
Vue.component('Datefield',Datefield);
Vue.component('Numberfield',Numberfield);
Vue.component('Numberfield2',Numberfield2);
Vue.component('Uploadfield',Uploadfield);
Vue.component('Popupfield',Popupfield);
Vue.component('Selectfield',Selectfield);
Vue.component('Timefield',Timefield);
Vue.component('Ratingfield',Ratingfield);
Vue.component('Tagfield',Tagfield);
Vue.component('Spinnerfield',Spinnerfield);
Vue.component('QDataTable',QDataTable);
Vue.component('Datatable',Datatable);
Vue.component('Detail',Detail);
Vue.component('DatatableMulti',DatatableMulti);
Vue.component('Preview', Preview);
Vue.component('Form',Form);
  
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize("en", en);
// vee-validate#
Vue.config.performance = true
Vue.prototype.$log = console.log.bind(console)

mixin.methods['mixin1'] = ()=> {return mixin1.methods };
mixin.methods['mixin2'] = ()=> {return mixin2.methods };
mixin.methods['mixin3'] = ()=> {return mixin3.methods };
mixin.methods['mixin4'] = ()=> {return mixin4.methods };
mixin.methods['mixin5'] = ()=> {return mixin5.methods };
mixin.methods['mixin6'] = ()=> {return mixin6.methods };
mixin.methods['mixin7'] = ()=> {return mixin7.methods };
mixin.methods['mixin8'] = ()=> {return mixin8.methods };

Vue.mixin(mixin)
let vm =new Vue({
  el: '#app',
  router,
  store,
  // icons: { freeSet },
  icons,
  i18n,
  template: '<App/>',
  components: {
    App
  },
  created(){
    document.title=this.appName;
    if( (window.location.href).includes('/register') ){
      return;
    }
    let headers  = {
      "Content-Type": "Application/json"
    };
    if(this.apiServer.toLowerCase()==='apache'){
      headers['Authorization'] = localStorage.token;
    }else{
      headers['UserToken'] = localStorage.token;
    }
    this.$store.commit('set',["apiHeader",headers]);
    this.$store.commit('addRoute',mixin1.routes);
    this.$store.commit('addRoute',mixin2.routes);
    this.$store.commit('addRoute',mixin3.routes);
    this.$store.commit('addRoute',mixin4.routes);
    this.$store.commit('addRoute',mixin5.routes);
    this.$store.commit('addRoute',mixin6.routes);
    this.$store.commit('addRoute',mixin7.routes);
    this.$store.commit('addRoute',mixin8.routes);
    this.$store.dispatch('registerMenuExecution');
    this.$router.addRoutes([{
      path: "/",
      redirect: "/dashboard",
      name: "Home",
      component:() => import("@/containers/TheContainer"),
      children:this.$store.state.routes
    }]);
    this.apiCheckAuth(this.apiAfterLogin);
  },
})
global.vm = vm;

