export default class DecoderHelper{
    constructor(){
        return this;
    }
    Date(){
        let tanggal = new Date();
        return `${tanggal.getDate()}/${tanggal.getMonth()+1}/${tanggal.getFullYear()}`;
    }
    formatUSD(value){
        Ext.util.Format.thousandSeparator=",";
        return Ext.util.Format.usMoney(value);
    }
    formatIDR(value){
        Ext.util.Format.thousandSeparator=".";
        return Ext.util.Format.currency(value,"Rp",".",""," ")
    }
    thousand(value,prefix="",postfix=""){
       return prefix+(parseFloat(parseFloat(value).toFixed(2))).toLocaleString(undefined,{ minimumFractionDigits: 2 }) + postfix;
    }
    upperWord(str,splitter="_",toSplitter=" "){
        var sentence = "";
        (str.split(splitter)).forEach(function(word){
            sentence+= (sentence!==""?toSplitter:"")+word.toLowerCase().charAt(0).toUpperCase() + word.substr(1).toLowerCase();
        });
        return sentence;
    }
    textOrNumber(value){
        return Ext.Number.from(value,null)==null?value:this.thousand(value);
    }
    toUpperCase(value){
        return value.toUpperCase();
    }
    toLowerCase(value){
        return value.toLowerCase();
    }
    toDateServer(value){
        let data = value.split("/");
        return data[2]+"-"+data[1]+"-"+data[0];
    }
    getNameToday( datestring = null){
        var presentDate;
        presentDate = (datestring === null || datestring === undefined)
                    ? new Date()
                    : new Date(datestring);

        let weekday = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'][ presentDate.getDay() ];
        return weekday;
    }
    toNumber(value){
        return value == null ? 0 : parseInt(value);
    }
}