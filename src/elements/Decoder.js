export default class Decoder{
    constructor(rawData) {
        this.config = {
            type            : "header",
            data            : {},
            controller      : 'qlcontroller',
            formName        : null,
            rowSpaces       : [],
            indexJSON       : 0,
            title           : null,
            id              : null,
            rowSpaces       : [],
            newHbox         : [],
            isOnline        : false,
            cols            : 2,
            defaultWidth    : '100%',
            labelWidth      : '100%',
            convertedFields : [],
            properties      : [],
            buttons         : [],
            buttonAlign     : "center",
            grid            : null,
            margin          : "0 0 0 0",
            aliases         : []
        };
        this.config.data = rawData;
    }
    getData(){ return this.config.data }
    getRowSpaces(){ return this.config.rowSpaces }
    setRowSpaces(val){ return this.config.rowSpaces=val; }
    getProperties(){ return this.config.properties }
    setProperties(val){ this.config.properties=val; }
    setCols(val){ this.config.cols=val; }
    getCols(){ return this.config.cols }
    setIndexJSON(val){ return this.config.indexJSON=val; }
    getIndexJSON(){ return this.config.indexJSON }
    getDefaultWidth(){ return this.config.defaultWidth }
    getLabelWidth(){ return this.config.labeltWidth }
    getConvertedFields(){ return this.config.convertedFields }
    getMargin(){ return this.config.margin }
    get(){
        let self=this;
        var data = self.getData().forms[self.getIndexJSON()];
        var jsonVersion = self.getData()['json_version']===undefined?1:self.getData()['json_version'];
        var semuaKolom = [];
        var columns = data.data;
        var colsTemp = columns;
        var columns  = [];
        var hiddenfields = [];
        var joining_id = null;
        var separators = [];
        for(let i=0; i<colsTemp.length; i++){
            if(jsonVersion==2){
                colsTemp[i]['label'] = (colsTemp[i]["label-name-type"]).split("*-*")[0];
                colsTemp[i]['name'] = (colsTemp[i]["label-name-type"]).split("*-*")[1];
                colsTemp[i]['type'] = (colsTemp[i]["label-name-type"]).split("*-*")[2];
                colsTemp[i]['required'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[1]=="true"?true:false;
                colsTemp[i]['readonly'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[2]=="true"?true:false;
                colsTemp[i]['disabled'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[3]=="true"?true:false;
                colsTemp[i]['hidden'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[4]=="true"?true:false;
                colsTemp[i]['withlabel'] = colsTemp[i]['withlabel']!==undefined?colsTemp[i]['withlabel']:true;
            }
           if(colsTemp[i].label==''||colsTemp[i].label==null){
                if(joining_id==null){
                    joining_id = i-1;
                    colsTemp[joining_id]['child']=[];
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }else{
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }
           }else{
               joining_id=null;
           }
        }
        columns = colsTemp.filter(function(data,index){            
            if( (data['type']).includes('separator')){
                let before = colsTemp[index-1];
                Object.assign(data,{
                    index:index
                });
                if(before==undefined){
                    separators.push(data);
                }else if(!(before.type).includes('separator')){
                   separators.push(data);
                }else{
                    if(separators.length==0){
                        separators.push(data);
                    }else{
                        data['index']=separators[separators.length-1].index;
                        separators.push(data);
                    }
                }
            }
            if(data.type=="hidden"){
                hiddenfields.push(data);
            }
            if(data.label!=''&& data.label!=null && data.type!="hidden" && !(data.type).includes('separator')){
                return data;
            }
        });

        var inlineRowSpaces = [];
        var hitung=0;
        for(let i=0;i<columns.length ;i++){
            for(let j=0;j<self.getRowSpaces().length;j++){
                if( self.getRowSpaces()[j].id == columns[i].id ){
                    for(let k=0;k<self.getRowSpaces()[j].total;k++){
                        inlineRowSpaces.push(i+( (self.getRowSpaces()[j].position=="before")?1:2)+(hitung++) );
                    }
                    break;
                }
            }
        }
        inlineRowSpaces.forEach(function(dt,index){
            columns.splice( (index-1), 0, {
                "name": "rowspaces"+index,
                "type": "rowspaces",
                "label": "rowspaces",
            });
        });
        if(columns.length%self.getCols() > 0){
            columns.push({
                "name": "rowspacesother",
                "type": "rowspaces",
                "label": "rowspaces",
            });
        }
        var jumlahBaris = Math.ceil( columns.length/self.getCols() );
        for (let i = 0; i < jumlahBaris ; i++) {
            var hasilFields = [];
            if( columns[i].child == undefined ){
                hasil =  self.kamusElement(columns[i]);
            }else{
                var hasil = {
                    component   : 'containerfield',
                    cls     : "qlsandingan",
                    required: columns[i].required,
                    labelWidth:self.getLabelWidth(),
                    isField : false,
                    layout  : 'hbox',
                    labelWrap:true,
                    hidden:columns[i].hidden,
                    label  : columns[i].label
                }
                var oldLabel = columns[i].label;
                var items = [];
                let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[i].type=='label only'?0:1))-3}%`
                self.tempWidth=defaultWidth;

                if(!["label only"].includes(columns[i].type)){
                    columns[i].withlabel=false;
                    var childArr=self.kamusElement(columns[i],true);
                    items.push( Object.assign(
                        childArr[0],{
                            style:{
                                "margin-left":"0px",
                                "max-height": !["textarea","popup"].includes(columns[i].type)?"38px":"100%"
                            }
                        })
                    );
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                for(let j=0;j<columns[i].child.length;j++){
                    var childArr=self.kamusElement(columns[i].child[j],true);
                    var child=childArr[0];
                    if(!["label only"].includes(columns[i].type) || j>0){
                        Object.assign(child,{
                            style:{
                                "margin-left":"5px",
                                "max-height":!["textarea","popup"].includes(columns[i].child[j].type)?"38px":"100%"
                            }
                        });
                    }
                    items.push(child);
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                items.unshift({
                    component:"Labelfield",
                    label:oldLabel
                })
                hasil['items']=items;
            }
            hasilFields.push(hasil);
            try{
                if( columns[jumlahBaris+i].child ==undefined ){
                    var hasil = self.kamusElement(columns[jumlahBaris+i]);
                    hasil['kanan']=true;
                }else{
                    var hasil = {
                        component   : 'containerfield',
                        cls     : "qlsandingan",
                        required: columns[jumlahBaris+i].required,
                        labelWidth:self.getLabelWidth(),
                        layout  : 'hbox',
                        labelWrap : true,
                        hidden:columns[jumlahBaris+i].hidden,
                        label  : columns[jumlahBaris+i].label
                    }
                    var items = [];
                    let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[jumlahBaris+i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[jumlahBaris+i].type=='label only'?0:1))-3}%`
                    self.tempWidth=defaultWidth;

                    if(!["label only"].includes(columns[jumlahBaris+i].type)){
                        columns[jumlahBaris+i].withlabel=false;
                        var childArr=self.kamusElement(columns[jumlahBaris+i],true);
                        items.push( Object.assign(
                            childArr[0],{
                                // flex:1,
                                style:{
                                    "margin-left":"0px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].type)?"38px":"100%"
                                }
                            })
                        );
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    for(let j=0;j<columns[jumlahBaris+i].child.length;j++){
                        var childArr=self.kamusElement(columns[jumlahBaris+i].child[j],true);
                        var child=childArr[0];
                        if(!["label only"].includes(columns[jumlahBaris+i].type) || j>0){
                            Object.assign(child,{
                                style:{
                                    "margin-left":"5px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].child[j].type)?"38px":"100%"
                                }
                            });
                        }
                        items.push(child);
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    items.unshift({
                        component:"Labelfield",
                        label:hasil.label
                    });
                    hasil['items']=items;
                }
                hasil["style"]={ "margin-left" : "30px"}
                hasilFields.push( hasil );
            }catch(err){
                
            }
            let separatorArray = separators.filter(_separator=>{
                if(self.getCols()==2){
                    return _separator.index/2==i;
                }
                return _separator.index==i;
            });

            separatorArray.forEach(_separator=>{                
                let separatorElem={};
                if(_separator.type=='separator_line'){
                    separatorElem={
                        component:'separator_line',
                        label:"<hr size='1px' style='border: 1px solid #d2c9c9;'>"
                    }
                }else if(_separator.type=='separator_label'){
                    separatorElem={
                        component:'separator_label',
                        label:`${_separator['label']}`
                    }
                }
                semuaKolom.push( {
                    component:"qlbarisfield",
                    // flex:1,
                    items : [separatorElem],
                } );
            })

            semuaKolom.push( {
                component:"qlbarisfield",
                items : hasilFields,
            } );
        }
        hiddenfields.forEach(function(dt){
            semuaKolom.push(
                self.kamusElement(dt)
            );
        });
        return semuaKolom;
    }
  
    kamusElement(column,isChild=false){
        let self = this;
        let perluTextHidden=true;
        if(column['name']!=null && column['name']!=undefined){
            column.name=column.name.toLowerCase();
        }
        if(column.label==""||column.label==null||column.label==undefined){
            column.label="";
        }
        var defaultWidth = isChild?"100%":self.getDefaultWidth();
        if(column.withlabel != undefined && column.withlabel != null ){
            column.label = column.withlabel? column.label:"";
        }
        var convertMe = false;
        var elements;
        for(let index=0; index<self.getConvertedFields().length;index++){
            if( self.getConvertedFields()[index].id == column.id ){
                convertMe = true;
                elements = self.getConvertedFields()[index].fields;
                break;
            }
        }
        if(convertMe){
            return {
                    xtype:'container',
                    autoSize:true,
                    cls:'qlfield',
                    // padding: "1 1 1 1",
                    margin: "1 30 4 5", //TOP RIGHT BOTTOM LEFT
                    items:elements
            };
        }

        var data;
        if(["enum","select","enum_search","search"].includes(column.type)){
            // if(column.name!==null){
            //     column.name = column.name.replace("_id","");
            // }
            data = {
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component: 'Selectfield'
            };
        }else if(["enum_popup","popup"].includes(column.type) || column.name=='___'){
            // if(column.name!==null){
            //     column.name = column.name.replace("_id","");
            // }
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Popupfield'
            };

        }else if(["tagfield","tag"].includes(column.type) || column.name=='___'){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Tagfield'
            };

        }else if(["enum_radio","radio"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Radiofield'
            };
            // if(data["default"]!==null){
            //     data["value"] = data["default"];
            // }
        }else if(["enum_checkbox","checkbox","check one","checkone","array","check many","checkmany"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                checkone: ['check one','checkone'].includes(column.type.toLowerCase()),
                component:'Checkboxfield'
             };
            // data["items"] = data.items[0].value;
        }else if(["check-labelright"].includes(column.type)){
            data={
                xtype : "container",
                hidden: column.hidden,
                width: isChild? self.tempWidth:defaultWidth,
                layout:"hbox",
                items : [{
                    xtype    : "checkboxfield",
                    style    : "margin-right:4px",
                    required : column.required,
                    readOnly : column.readonly,
                    disabled : column.disabled,
                    name     : column.name,
                    checked  : column.default=="true"||column.default===true?true:false,
                },{
                    xtype:"component",
                    html:`<div style='padding-top:5px'>${column.label}</div>`
                }],
                cls: "qlchecklabelright",
            }
        }else if(['file_image',"file","upload","image","filefield","file one","files many"].includes(column.type)){
            data= {
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Uploadfield'
            };
        }else if(["money","numeric","number"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Numberfield'
            };
        }else if(["date","datetime"].includes(column.type)){
            let value = column.value;
            if(typeof(value)=='string' && value.toLowerCase()=='now'){
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                value = yyyy + '-' + mm + '-' + dd;
            }
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:value,
                component:'Datefield'
            };
        }else if(["time"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Timefield'
            };
        }else if(["rowspaces","space"].includes(column.type)){
            data ={
                label:"",
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Space'
            };
        }else if(["text_only","label","label_only","label only","text only"].includes(column.type)){
            data ={
                cls: "qllabel",
                html   : `<span>${column.label}</span>`,
                style:{
                    "font-size":"14px",
                    "color": "#656f75"
                }
            };
        }else if(["textnumber"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Textnumberfield'
            };
        }else if(["textarea"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Textareafield'
            };
        }else if(["password"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Passwordfield'
            };
        }else if(["domainfield"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Domainfield'
            };
        }else if(["string","varchar","text"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Textfield'
            };
        }else if(["email"].includes(column.type)){
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Emailfield'
            };
        }else if(["hidden"].includes(column.type)){
            data ={
                name: column.name,
                xtype: 'textfield',
                hidden:true
            };
        }else{
            data ={
                label:column.label,
                required:column.required,
                readonly:column.readonly,
                hidden:column.hidden,
                name:column.name,
                value:column.value,
                component:'Textfield'
            };
        }
        let props = self.getProperties();
        for(let key in props){
            if(key == column.name){
                Object.assign(data,props[key]);
                break;
            }
        }
        var fixedItems = data;
        if(["hidden"].includes(column.type)){
            return data;
        }
        return isChild?[fixedItems]:fixedItems;
    }  
}
