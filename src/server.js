const settings = {
    appName: "GOES",
    apiServer : "IIS", /*  APACHE atau IIS */
    rootData: "model", //Apache pakai 'data' dan IIS pakai 'model'
    // apiOperationUrl:"https://backend.dejozz.com/larahan/public/operation/",
    // apiLoginUrl: "https://backend.dejozz.com/larahan/public/login",
    // apiRegisterUrl: "https://backend.dejozz.com/larahan/public/register",
    // apiCheckAuthUrl: "https://backend.dejozz.com/larahan/public/me",
    
    apiOperationUrl:"http://118.99.67.130:83/goeswebapi/api",
    apiLoginUrl: "http://118.99.67.130:83/goeswebapi/api/login",
    apiRegisterUrl: "https://backend.dejozz.com/pcidev/public/register",
    apiCheckAuthUrl: "http://118.99.67.130:83/goeswebapi/api/getme",

    apiParameters: function(server){
        if(server.toLowerCase()==='apache'){
            return {
                paginate : "paginate",
                page     : "page",
                current_page:"current_page",
                page_end : "last_page",
                filter_fields : "searchfield",
                filter_text   : "search",
                order_by : "orderBy",
                order_type: "orderType",
                select_fields : "selectField"
            }
        }
        return {
            paginate : "pageSize", 
            page     : "pageNumber",
            current_page:"pageNumber",
            page_end : "pageCount",
            filter_fields : "filterField",
            filter_text   : "filterText",
            order_by : "orderField",
            order_type: "orderType",
            select_fields : "selectField"
        }
    },
    setPayloadLogin(data){
        if(vm.apiServer.toLowerCase()==='apache'){
            return {
                email:data.username,
                password:data.password
            }
        }
        return {
            m_user_login:data.username,
            m_user_password:data.password
        }
    },
    getUserDataAfterLogin:function(data){
        let currentHeader = vm.$store.state.apiHeader;
        
        if(vm.apiServer.toLowerCase()==='apache'){
            currentHeader['Authorization'] = data.token_type+" "+data.token;
            window.localStorage.token=data.token_type+" "+data.token;
        }else{
            currentHeader['UserToken'] = data.model.userToken;
            window.localStorage.token=data.model.userToken;
        }
        vm.$store.commit("set",["apiHeader",currentHeader]);
        vm.apiAfterLogin();
        console.log(data,"login success")
    },
    getUserData:function(data){
        let userData = null
        if(vm.apiServer.toLowerCase()==='apache'){
            localStorage.userdata=JSON.stringify(data);
            userData = data;
        }else{
            localStorage.userdata=JSON.stringify(data.model.dtUser[0]);
            userData = data.model.dtUser[0];
        }
        vm.$store.commit("set",["user",userData])
    },
    handleError: function(error) {
        console.log(error,'error')
        if(vm.apiServer.toLowerCase()==='apache'){
            vm.$alertify.alertWithTitle("Warning!", error);       
        }else{
            let key_error = Object.keys(error);
            let message_error = "";
            for (let index = 0; index < key_error.length; index++) {
                let message = error[key_error[index]][0];
                console.log(message);
                message_error += message.replace("undefined", key_error[index]).replace("null", key_error[index])+"<br>"; 
            }
            vm.$alertify.alertWithTitle("Warning!", message_error);
        }
    },
    afterLogin(ctx){
        return;
        let loader = ctx.$loading.show({
            "z-index": 999999,
        });
        fetch(ctx.apiOperationUrl+"backend_roles/getroles", {
            method: "GET",
            headers: ctx.$store.state.apiHeader,
        }).then((response, body) => {
            if (!response.ok) {
                try {
                    return response
                        .text()
                        .then((err) => Promise.reject(err));
                } catch (e) {
                    return response
                        .json()
                        .then((err) => Promise.reject(err));
                }
            }
            return response.json();
        }).then((data) => {
            ctx.$store.commit("set",['routesAllowed', data]);
            loader.hide();
        }).catch((error) => {
            ctx.$alertify.alertWithTitle(
                "Warning!",
                "Failed to Get List of Menu!"
            );
            loader.hide();
        });

        fetch(ctx.apiOperationUrl+"backend_user/getme", {
            method: "GET",
            headers: ctx.$store.state.apiHeader,
        }).then((response, body) => {
            if (!response.ok) {
                try {
                    return response
                        .text()
                        .then((err) => Promise.reject(err));
                } catch (e) {
                    return response
                        .json()
                        .then((err) => Promise.reject(err));
                }
            }
            return response.json();
        }).then((data) => {
            ctx.$store.commit("set",['user', data]);
            let responsibility;
            if(localStorage.responsibility){
                responsibility = JSON.parse(localStorage.responsibility);
            }else{
                responsibility = {
                    io: data.io,
                    io_id: data.io_id,
                    is_primary: data.is_primary,
                    region: data.region,
                    region_id: data.region_id,
                    responsibility: data.responsibility,
                    responsibility_id: data.responsibility_id,
                };
            }
            ctx.$store.commit("set",['responsibility', responsibility]);
            localStorage.userpci=JSON.stringify(data);
            Object.assign(data, responsibility)
            let currentHeader = ctx.$store.state.apiHeader;
            currentHeader['USERPCIDATA'] = JSON.stringify(data);
            ctx.$store.commit("set",["apiHeader",currentHeader]);
        }).catch((error) => {
            ctx.$alertify.alertWithTitle(
                "Warning!",
                "Maybe Server Error!"
            );
        });
    }
};
export default settings;
