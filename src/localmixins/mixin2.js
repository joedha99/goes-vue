const localmixin ={
    routes :[
        {
            modul: 'Setup',
            submodul: 'Master',
            icon: "cil-caret-right",
            text: "Warehouse",
            seq: 1,
            path: "/setup/master_warehouse",
            name: "Warehouse",
            component: () => import("@/views/Setup/Master/Warehouse/Landing.vue")
        },{
            modul: 'Setup',
            submodul: 'Master',
            path: "/setup/master_warehouse/:id",
            name: "Warehouse",
            component: () => import("@/views/Setup/Master/Warehouse/Form.vue")
        },
        {
            modul: 'Setup',
            submodul: 'Master',
            icon: "cil-caret-right",
            text: "User",
            seq: 1,
            path: "/setup/master_user",
            name: "User",
            component: () => import("@/views/Setup/Master/User/Landing.vue")
        },{
            modul: 'Setup',
            submodul: 'Master',
            path: "/setup/master_user/:id",
            name: "User",
            component: () => import("@/views/Setup/Master/User/Form.vue")
        },
        {
            modul: 'Purchasing',
            submodul: 'Transaction',
            icon: "cil-caret-right",
            text: "Internal Order",
            seq: 1,
            path: "/purchasing/transaction_internal_order",
            name: "Internal Order",
            component: () => import("@/views/Purchasing/Transaction/Internal Order/Landing.vue")
        },{
            modul: 'Purchasing',
            submodul: 'Transaction',
            path: "/purchasing/transaction_internal_order/:id",
            name: "Internal Order",
            component: () => import("@/views/Purchasing/Transaction/Internal Order/Form.vue")
        },{
            modul: 'Fixed Asset',
            submodul: 'Master',
            icon: "cil-caret-right",
            text: "Asset Category",
            seq: 1,
            path: "/fixed_asset/master_asset_category",
            name: "Asset Category",
            component: () => import("@/views/Fixed Asset/Master/Asset Category/Landing.vue")
        },{
            modul: 'Fixed Asset',
            submodul: 'Master',
            path: "/fixed_asset/master_asset_category/:id",
            name: "Asset Category",
            component: () => import("@/views/Fixed Asset/Master/Asset Category/Form.vue")
        }
    ],
    methods:{}
}
export default localmixin