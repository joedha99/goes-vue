const localmixin ={
    routes :[
    //Setup
        {
        modul: 'Setup',
        submodul: 'Master',
        icon: "cil-caret-right",
        text: "Business Unit",
        seq: 1,

        path: "/setup/master_business_unit",
        name: "Business Unit",
        component: () => import("@/views/Setup/Master/Business Unit/Landing.vue")
    },{
        modul: 'Setup',
        submodul: 'Master',

        path: "/setup/master_business_unit/:id",
        name: "Business Unit",
        component: () => import("@/views/Setup/Master/Business Unit/Form.vue")
    },
    {
        modul: 'Setup',
        submodul: 'Master',
        icon: "cil-caret-right",
        text: "User Role",
        seq: 1,

        path: "/setup/master_user_role",
        name: "User Role",
        component: () => import("@/views/Setup/Master/User Role/Landing.vue")
    },
    {
        modul: 'Setup',
        submodul: 'Master',

        path: "/setup/master_user_role/:id",
        name: "User Role",
        component: () => import("@/views/Setup/Master/User Role/Form.vue")
    },

    //Marketing
    {
        modul: 'Marketing',
        submodul: 'Transaction',
        icon: "cil-caret-right",
        text: "Shipment/Surat Jalan",
        seq: 1,

        path: "/marketing/transaction_shipment",
        name: "Shipment",
        component: () => import("@/views/Marketing/Transaction/Shipment/Landing.vue")
    },
    {
        modul: 'Marketing',
        submodul: 'Transaction',

        path: "/marketing/transaction_shipment/:id",
        name: "Shipment",
        component: () => import("@/views/Marketing/Transaction/Shipment/Form.vue")
    },
    ],
    methods:{
        getCreatedAt:function(){
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1; 
            let yyyy = today.getFullYear();
            let hh = today.getHours();
            let ii = today.getMinutes();
            let ss = today.getSeconds();
            if(dd<10) 
            {
                dd='0'+dd;
            }
            if(mm<10) 
            {
                mm='0'+mm;
            }
            if(hh<10) 
            {
                hh='0'+hh;
            }
            if(ii<10) 
            {
                ii='0'+ii;
            }
            if(ss<10) 
            {
                ss='0'+ss;
            }
            var created_at = mm+"/"+dd+"/"+yyyy+" "+hh+":"+ii+":"+ss;
            return created_at;
        },
        apiChangeStatus(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            fetch(vm.apiOperationUrl + payload.model + "/" + payload.id, {
                method: "PATCH",
                headers: vm.$store.state.apiHeader,
                body: JSON.stringify(payload.data),
            })
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            vm.$router.push({ name: "Login" });
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    vm.$alertify.success("Successfully Send Approval!");
                    loader.hide();
                    success(data);
                })
                .catch((error) => {
                    let errorMsg = "Failed to Send Approval!";
                    if (
                        typeof error == "object" &&
                        error.errors !== undefined
                    ) {
                        var errorMessage = "";
                        error.errors.forEach(function(errorSingle) {
                            let errorString = errorSingle.split(".")[0];
                            if (
                                errorString.includes("The ") &&
                                errorSingle.includes(payload.model)
                            ) {
                                errorString = errorString.split("]The ")[1];
                                let fieldName = !errorString.includes(" field ")
                                    ? errorString.split(" ")[0]
                                    : errorString
                                          .split(" field")[0]
                                          .replace(/ /g, "_");
                                // let field = fields[fieldName];
                                // if (field !== undefined) {
                                //     field.setError(errorString);
                                // }
                            }
                            errorMessage += errorString + "<br>";
                        });
                        if (errorMessage != "") {
                            errorMsg = errorMessage;
                        }
                    }
                    vm.$alertify.alertWithTitle("Warning!", errorMsg);
                    loader.hide();
                    errorFn(error);
                });
        },
    }
}
export default localmixin