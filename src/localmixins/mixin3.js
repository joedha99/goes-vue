const localmixin ={
    routes :[          
        {
            modul: 'Production',
            submodul: 'Transaction',
            icon: "cil-caret-right",
            text: "Order Subcon",
            seq: 1,
            path: "/production/transaction_ordersubcon",
            name: "Order Subcon",
            component: () => import("@/views/Production/Transaction/Order Subcon/Landing.vue")
        },{
            modul: 'Production',
            submodul: 'Transaction',
            path: "/production/transaction_ordersubcon/:id",
            name: "Order Subcon",
            component: () => import("@/views/Production/Transaction/Order Subcon/Form.vue")
        },
        {
            modul: 'Fixed Asset',
            submodul: 'Transaction',
            icon: "cil-caret-right",
            text: "Item Usage for AIP",
            seq: 1,
            path: "/Fixed Asset/transaction_itemusageforaip",
            name: "Item Usage for AIP",
            component: () => import("@/views/Fixed Asset/Transaction/Item Usage for AIP/Landing.vue")
        },{
            modul: 'Fixed Asset',
            submodul: 'Transaction',
            path: "/Fixed Asset/transaction_itemusageforaip/:id",
            name: "Item Usage for AIP",
            component: () => import("@/views/Fixed Asset/Transaction/Item Usage for AIP/Form.vue")
        }
    ],
    methods:{
        getCreatedAt:function(){
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1; 
            let yyyy = today.getFullYear();
            let hh = today.getHours();
            let ii = today.getMinutes();
            let ss = today.getSeconds();
            if(dd<10) 
            {
                dd='0'+dd;
            }
            if(mm<10) 
            {
                mm='0'+mm;
            }
            if(hh<10) 
            {
                hh='0'+hh;
            }
            if(ii<10) 
            {
                ii='0'+ii;
            }
            if(ss<10) 
            {
                ss='0'+ss;
            }
            var created_at = mm+"/"+dd+"/"+yyyy+" "+hh+":"+ii+":"+ss;
            return created_at;
        },
        apiChangeStatus(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            fetch(vm.apiOperationUrl + payload.model + "/" + payload.id, {
                method: "PATCH",
                headers: vm.$store.state.apiHeader,
                body: JSON.stringify(payload.data),
            })
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            vm.$router.push({ name: "Login" });
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    vm.$alertify.success("Successfully Send Approval!");
                    loader.hide();
                    success(data);
                })
                .catch((error) => {
                    let errorMsg = "Failed to Send Approval!";
                    if (
                        typeof error == "object" &&
                        error.errors !== undefined
                    ) {
                        var errorMessage = "";
                        error.errors.forEach(function(errorSingle) {
                            let errorString = errorSingle.split(".")[0];
                            if (
                                errorString.includes("The ") &&
                                errorSingle.includes(payload.model)
                            ) {
                                errorString = errorString.split("]The ")[1];
                                let fieldName = !errorString.includes(" field ")
                                    ? errorString.split(" ")[0]
                                    : errorString
                                          .split(" field")[0]
                                          .replace(/ /g, "_");
                                // let field = fields[fieldName];
                                // if (field !== undefined) {
                                //     field.setError(errorString);
                                // }
                            }
                            errorMessage += errorString + "<br>";
                        });
                        if (errorMessage != "") {
                            errorMsg = errorMessage;
                        }
                    }
                    vm.$alertify.alertWithTitle("Warning!", errorMsg);
                    loader.hide();
                    errorFn(error);
                });
        },
    }
}
export default localmixin