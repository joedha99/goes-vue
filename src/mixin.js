import settings from "./server";
import DecoderPdf from "@/elements/DecoderPdf";
export default {
    data() {
        return {
            appName: settings.appName,
            apiOperationUrl: settings.apiOperationUrl,
            apiLoginUrl: settings.apiLoginUrl,
            apiRegisterUrl: settings.apiRegisterUrl,
            apiCheckAuthUrl: settings.apiCheckAuthUrl,
            rootData: settings.rootData,
            apiServer: settings.apiServer,
            apiParameters : settings.apiParameters(settings.apiServer)
        };
    },
    computed:{
        window:()=>{
            return window;
        }
    },
    methods: {
        setPayloadLogin(data){
            return settings.setPayloadLogin(data)
        },
        setProps(props){ //mutate props per element
            for(let key in props){
                this[key+"_"] = props[key];
                if(key=="value"){
                    this.onchangeOriginal({type:"change"});
                }
            }
            return true;
        },
        getParent(el,name){
            if(el['$parent']!==undefined){
                if( typeof(el['$parent'].$options.name) == 'string' && (el['$parent'].$options.name).toLowerCase().includes(name.toLowerCase()) ){
                    return el['$parent'];
                }else if(typeof(el['$parent'].$options.name) == 'string'){
                    return this.getParent(el['$parent'], name.toLowerCase());
                }
            }else{
                return null;
            }
        },
        previewPdf(data) {
            let preview = new DecoderPdf();
            preview.pdfRefresh(data);
        },
        firstUpperCase(kata) {
            return kata.charAt(0).toUpperCase() + kata.slice(1);
        },
        toDateServer: function(value) {
            let data = value.split("/");
            return data[2] + "-" + data[1] + "-" + data[0];
        },
        apiGetParams(params) {
            let keys = Object.keys(params);
            var parameters = "";
            keys.forEach(function(key) {
                console.log(params[key]);
                var value = params[key];
                parameters += `${key}=${value}&`;
            });
            return parameters;
        },
        apiCreate(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            fetch(this.apiOperationUrl + payload.model, {
                method: "POST",
                headers: this.$store.state.apiHeader,
                body: JSON.stringify(payload.data),
            })
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            vm.$router.push({ name: "Login" });
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    vm.$alertify.success("Successfully Post Data!");
                    loader.hide();
                    success(data);
                })
                .catch((error) => {
                    let errorMsg = "Failed to Post Data!";
                    if (
                        typeof error == "object" &&
                        error.errors !== undefined
                    ) {
                        var errorMessage = "";
                        try{
                            error.errors.forEach(function(errorSingle) {
                                let errorString = errorSingle.split(".")[0];
                                if (
                                    errorString.includes("The ") &&
                                    errorSingle.includes(payload.model)
                                ) {
                                    errorString = errorString.split("]The ")[1];
                                    let fieldName = !errorString.includes(" field ")
                                        ? errorString.split(" ")[0]
                                        : errorString
                                              .split(" field")[0]
                                              .replace(/ /g, "_");
                                    let field = fields[fieldName];
                                    if (field !== undefined) {
                                        field.setError(errorString);
                                    }
                                }
                                errorMessage += errorString + "<br>";
                            });
                        }catch(err){}
                    }
                    if (errorMessage != "") {
                        errorMsg = errorMessage;
                    }
                    vm.$alertify.alertWithTitle("Warning!", errorMsg);
                    loader.hide();
                    errorFn(error);
                });
        },
        apiDelete(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            this.$alertify.confirmWithTitle(
                "Warning!",
                "Are You Sure to Remove Data?",
                () => {
                    let loader = vm.$loading.show({
                        "z-index": 999999,
                    });
                    fetch(
                        this.apiOperationUrl + payload.model + "/" + payload.id,
                        {
                            method: "DELETE",
                            headers: this.$store.state.apiHeader,
                        }
                    )
                        .then((response) => {
                            if (!response.ok) {
                                if (response.status == 401) {
                                    vm.$router.push({ name: "Login" });
                                    return response
                                        .text()
                                        .then((err) => Promise.reject(err));
                                }
                                try {
                                    return response
                                        .json()
                                        .then((err) => Promise.reject(err));
                                } catch (e) {
                                    return response
                                        .text()
                                        .then((err) => Promise.reject(err));
                                }
                            }
                            return response.json();
                        })
                        .then((data) => {
                            this.$alertify.success("Successfully Delete Data!");
                            loader.hide();
                            success(data);
                        })
                        .catch((error) => {
                            console.log(error);
                            loader.hide();
                            vm.$alertify.alertWithTitle(
                                "Error!",
                                "Failed Delete Data!"
                            );
                            errorFn(error);
                        });
                }
            );
        },
        apiUpdate(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            fetch(this.apiOperationUrl + payload.model + "/" + payload.id, {
                method: "PUT",
                headers: this.$store.state.apiHeader,
                body: JSON.stringify(payload.data),
            })
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            vm.$router.push({ name: "Login" });
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    this.$alertify.success("Successfully Update Data!");
                    loader.hide();
                    success(data);
                })
                .catch((error) => {
                    let errorMsg = "Failed to Update Data!";
                    if (
                        typeof error == "object" &&
                        error.errors !== undefined
                    ) {
                        var errorMessage = "";
                        error.errors.forEach(function(errorSingle) {
                            let errorString = errorSingle.split(".")[0];
                            if (
                                errorString.includes("The ") &&
                                errorSingle.includes(payload.model)
                            ) {
                                errorString = errorString.split("]The ")[1];
                                let fieldName = !errorString.includes(" field ")
                                    ? errorString.split(" ")[0]
                                    : errorString
                                          .split(" field")[0]
                                          .replace(/ /g, "_");
                                // let field = fields[fieldName];
                                // if (field !== undefined) {
                                //     field.setError(errorString);
                                // }
                            }
                            errorMessage += errorString + "<br>";
                        });
                        if (errorMessage != "") {
                            errorMsg = errorMessage;
                        }
                    }
                    vm.$alertify.alertWithTitle("Warning!", errorMsg);
                    loader.hide();
                    errorFn(error);
                });
        },
        apiRead(payload, success = function(dt) {}, errorFn = function(dt) {}) {
            let loader;
            if (payload.loading !== undefined && payload.loading == false) {
            } else {
                loader = vm.$loading.show({
                    "z-index": 999999,
                });
            }
            let endPoint = payload.id === undefined ? "" : "/" + payload.id;

            let fixedUrl = this.apiOperationUrl + payload.model + endPoint;
            if (payload.parameters !== undefined) {
                fixedUrl += "?" + new URLSearchParams(payload.parameters);
            }
            fetch(fixedUrl, {
                method: "GET",
                headers: this.$store.state.apiHeader,
            })
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            vm.$router.push({ name: "Login" });
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    try {
                        return response.json();
                    } catch (e) {
                        return response;
                    }
                })
                .then((data) => {
                    if (loader) {
                        loader.hide();
                    }
                    success(data);
                })
                .catch((error) => {
                    // vm.$alertify.alertWithTitle('Warning!','Failed to Read Data!');
                    if (loader) {
                        loader.hide();
                    }
                    errorFn(error);
                });
        },
        apiCreateUpload: function(
            payload,
            callback = function(json) {},
            error = function(json) {},
            progress = function(evt) {}
        ) {
            let bigUrl = payload.model.includes("http")
                ? payload.model
                : `${this.apiOperationUrl}${payload.model}`;
            // let loader = vm.$loading.show({
            //     'z-index':999999
            // });
            var request = new XMLHttpRequest();
            request.open("POST", bigUrl);
            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    // loader.hide();
                    if (request.status == 200 || request.status == 201) {
                        callback(request.responseText);
                    } else {
                        error(request.responseText);
                    }
                }
            };
            (Object.keys(this.$store.state.apiHeader)).forEach(function(key){
                request.setRequestHeader(key,this.$store.state.apiHeader[key]);
            });
            request.upload.addEventListener("progress", progress);
            request.send(payload.data);
        },
        apiRequest(
            payload,
            success = function(dt) {},
            errorFn = settings.handleError
        ) {
            let loader;
            if (payload.loading !== undefined && payload.loading == false) {
            } else {
                loader = vm.$loading.show({
                    "z-index": 999999,
                });
            }
            let pay = {
                method: payload.method ? payload.method : "GET",
                headers: this.$store.state.apiHeader,
            };
            if (payload.method === "POST") {
                pay["body"] = JSON.stringify(payload.data);
            }
            fetch(this.apiOperationUrl + payload.model, pay)
                .then((response) => {
                    if (!response.ok) {
                        if (response.status == 401) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    if(response.headers.get("Content-Type") && !response.headers.get("Content-Type").toLocaleLowerCase().includes('json') ){
                        return response.blob();
                    }
                    return response.json();
                })
                .then((data) => {
                    success(data);
                })
                .catch((error) => {
                    errorFn(error);
                }).finally(()=>{                    
                    if (loader) {
                        loader.hide();
                    }
                });
        },
        apiLogin(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            let pay = {
                method: "POST",
                headers: {
                    "Content-Type": "Application/json",
                },
                body: JSON.stringify(payload),
            };
            fetch(this.apiLoginUrl, pay)
                .then((response, body) => {
                    if (!response.ok) {
                        try {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    loader.hide();
                    settings.getUserDataAfterLogin(data);
                    success(data);
                })
                .catch((error) => {
                    console.log(error);
                    vm.$alertify.alertWithTitle(
                        "Warning!",
                        "Maybe Server Error!"
                    );
                    loader.hide();
                    errorFn(error);
                });
        },
        apiCheckAuth(callback = function(dt) {}) {
            if (localStorage.token === undefined) {
                if(this.$route.name!=="Login"){
                    this.$router.push({ name: "Login" });                    
                }
                return;
            }
            let loader = this.$loading.show({
                "z-index": 999999,
            });
            let pay = {
                method: "GET",
                headers: this.$store.state.apiHeader,
            };
            fetch(this.apiCheckAuthUrl, pay)
                .then((response, body) => {
                    if (!response.ok) {                        
                        if (response.status == 401) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                        try {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    settings.getUserData(data);
                    loader.hide();
                    callback(data);
                })
                .catch((error) => {
                    loader.hide();
                    if(this.$route.name!=="Login"){
                        this.$router.push({ name: "Login" });                    
                    }
                });
        },
        apiRegister(
            payload,
            success = function(dt) {},
            errorFn = function(dt) {}
        ) {
            let loader = vm.$loading.show({
                "z-index": 999999,
            });
            let pay = {
                method: "POST",
                headers: {
                    "Content-Type": "Application/json",
                },
                body: JSON.stringify(payload),
            };
            fetch(this.apiRegisterUrl, pay)
                .then((response, body) => {
                    if (!response.ok) {
                        try {
                            return response
                                .json()
                                .then((err) => Promise.reject(err));
                        } catch (e) {
                            return response
                                .text()
                                .then((err) => Promise.reject(err));
                        }
                    }
                    return response.json();
                })
                .then((data) => {
                    loader.hide();
                    success(data);
                })
                .catch((error) => {
                    console.log(error);
                    // vm.$alertify.alertWithTitle('Warning!','Maybe Server Error!');
                    loader.hide();
                    errorFn(error);
                });
        },
        apiLogout() {
            window.localStorage.clear();
            // this.$router.push({ name: "Login" });
            window.location.href="/login"
        },
        apiAfterLogin(){
            settings.afterLogin(this);
        },
        setFormValues(values) {
            this["values"] = values;
            for (let ref in this.$refs) {
                if (this.$refs[ref]._name == "<Form>") {
                    this.$refs[ref].$forceUpdate();
                }
            }
        },
        getQueryString(params) {
            return Object.keys(params)
                .map((k) => {
                    if (Array.isArray(params[k])) {
                        return params[k]
                            .map(
                                (val) =>
                                    `${encodeURIComponent(
                                        k
                                    )}[]=${encodeURIComponent(val)}`
                            )
                            .join("&");
                    }
                    return `${encodeURIComponent(k)}=${encodeURIComponent(
                        params[k]
                    )}`;
                })
                .join("&");
        },
        getWidth(key, fields = null) {
            var fields = fields === null ? this.fields : fields;
            let ketemu = fields.find((dt) => key === dt.key);
            return `width:${ketemu.width}%`;
        },
        getItems(items) {
            console.log(items);
            let newData = items.filter((dt) => {
                let keys = Object.keys(dt);
                for (let i in keys) {
                    console.log(dt[i]);
                    dt[i] = dt[i] === undefined || dt[i] === null ? "" : dt[i];
                }
                return dt;
            });
            return newData;
            // console.log(newData);
        },
        setCreating(val) {
            try {
                this.isCreating = val;
                this.$refs.landing.load();
            } catch (e) {}
        },
        formatNumber(val, decimals = 0) {
            if (val === null || val == "") {
                val = 0;
                return;
            }
            if (val !== null && val != "" && val != ".") {
                let rawValue = val.toString();
                var decimal;
                if (rawValue.includes(".")) {
                    val = rawValue.split(".")[0].replace(/,/g, "");
                    decimal =
                        rawValue.split(".")[1].length == 0
                            ? "00000"
                            : rawValue.split(".")[1];
                    decimal = decimal.slice(0, decimals);
                } else {
                    val = rawValue.replace(/,/g, "");
                    decimal = "00";
                }
                let formattedVal = parseFloat(val).toLocaleString("en");
                val =
                    formattedVal +
                    (rawValue.includes(".")
                        ? `${decimals == 0 ? "" : "." + decimal}`
                        : "");
            }
            return val;
        },
        toNumber(val){
            let data=val;
            try{
                data =  parseFloat( (val.toString()).replace(/,/g,"") );
            }catch(e){
                data =  0;
            }
            return data;
        }
    },
};
