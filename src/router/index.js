import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/login',
      name: 'Login',
      component:() =>  import('@/views/Public/Login.vue')
    },
    {
      path: '/register',
      name: 'Register',
      component:() =>  import('@/views/Public/Register.vue')
    },
    {
      path: '/approval-structure',
      name: 'Approval Structure',
      component:() =>  import('@/views/Public/Register.vue')
    }
  ]
})
